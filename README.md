# CarCar

Team:

- Eduardo Silva - Sales
- Brandon Tiernay - Service

## Design

## Service microservice

The service microservice consists of three main models:

1. Technician model

   • The Technician model represents a service technician who can be assigned to appointments.

2. Appointment model

   • The Appointment model is used to manage appointments created within the service microservice.

3. AutomobileVO model

   • This model represents an Automobile Value Object (VO) and is used to store essential information about an automobile.

   • As a VO, it is a reference of the Automobile model found in the inventory microservice

The service microservice integrates with the inventory microservice in three main ways:

1. The service microservice interacts with the inventory microservice's Manufacturer and VehicleModel models through ForeignKeys.

2. The Manufacturer model in the inventory microservice is referenced by the VehicleModel model in the inventory microservice using a ForeignKey. This relationship provides information about the manufacturer of a specific vehicle model during the service appointment process.

3. The VehicleModel model in the inventory microservice is referenced by the Automobile model in the service microservice using a ForeignKey. This allows technicians to access detailed information about the vehicle model during service appointments.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

- Create a vehicle model
- Show a list of automobiles in inventory
- Create an automobile in inventory
- Created a Salesperson Model, View and Respective Form and List
- The form is able to synch information to the database from the FrontEnd
