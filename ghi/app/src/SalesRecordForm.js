import React, { useEffect, useState } from "react";

export const SalesRecordForm = () => {
	const [automobile, setAutomobile] = useState("");
	const [automobiles, setAutomobiles] = useState([]);
	const [salesperson, setSalesperson] = useState("");
	const [salespeople, setSalespeople] = useState([]);
	const [customer, setCustomer] = useState("");
	const [customers, setCustomers] = useState([]);
	const [price, setPrice] = useState("");

	const handleAutomobileChange = (event) => {
		const value = event.target.value;
		setAutomobile(value);
	};

	const handleSalespersonChange = (event) => {
		const value = event.target.value;
		setSalesperson(value);
	};

	const handleCustomerChange = (event) => {
		const value = event.target.value;
		setCustomer(value);
	};

	const handlePriceChange = (event) => {
		const value = event.target.value;
		setPrice(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.automobile = automobile;
		data.salesperson = salesperson;
		data.customer = customer;
		data.price = price;
		console.log(data);

		const url = "http://localhost:8090/api/sales/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			await response.json();

			setAutomobile("");
			setSalesperson("");
			setCustomer("");
			setPrice("");
		}
	};

	const fetchAutomobiles = async () => {
		const url = "http://localhost:8090/api/automobiles/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.automobiles);
		}
	};

	const fetchSalespeople = async () => {
		const url = "http://localhost:8090/api/salespeople/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople);
		}
	};

	const fetchCustomers = async () => {
		const url = "http://localhost:8090/api/customers/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers);
		}
	};

	useEffect(() => {
		fetchAutomobiles();
		fetchSalespeople();
		fetchCustomers();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4 forms">
					<h1>Record a new sale</h1>
					<form onSubmit={handleSubmit} id="create-Salesperson-form">
						<div className="form-floating mb-3">
							<select
								value={automobile}
								onChange={handleAutomobileChange}
								required
								name="automobile"
								id="automobile"
								className="form-select"
							>
								<option value="">Choose a automobile vin</option>
								{automobiles.map((automobile) => {
									return (
										<option key={automobile.id} value={automobile.id}>
											{`${automobile.vin}`}
										</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<select
								value={salesperson}
								onChange={handleSalespersonChange}
								required
								name="salesperson"
								id="salesperson"
								className="form-select"
							>
								<option value="">Choose a salesperson</option>
								{salespeople.map((salesperson) => {
									return (
										<option key={salesperson.id} value={salesperson.id}>
											{salesperson.name}
											{`${salesperson.first_name} ${salesperson.last_name}`}
										</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<select
								value={customer}
								onChange={handleCustomerChange}
								required
								name="customer"
								id="customer"
								className="form-select"
							>
								<option value="">Choose a customer</option>
								{customers.map((customer) => {
									return (
										<option key={customer.id} value={customer.id}>
											{`${customer.first_name} ${customer.last_name}`}
										</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								value={price}
								onChange={handlePriceChange}
								required
								type="number"
								name="price"
								id="price"
								className="form-control"
							/>
							<label>Price</label>
						</div>
						<button className="btn btn-dark buttons">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default SalesRecordForm;
