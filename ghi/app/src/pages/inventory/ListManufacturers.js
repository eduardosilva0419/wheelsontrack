import React, { useEffect, useState } from "react";

export const ListManufacturers = () => {
  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturers = async () => {
    const ListManufacturersUrl = "http://localhost:8100/api/manufacturers/";

    const response = await fetch(ListManufacturersUrl);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  // useEffects have two parameters: (1) an anonymous function and (2) a dependency array
  // useEffects do something at a given time
  useEffect(
    () => {
      // The something is the first parameter, an anonymous function
      fetchManufacturers();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  return (
    <div className="list-manufacturers-container">
      <h1>Manufacturers</h1>

      <table className="table table-striped manufacturers-list-table">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>

        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.name}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
