import React, { useEffect, useState } from "react";

export const CreateVehicleModel = () => {
  // Manufacturer Selection Data
  const [manufacturers, setManufacturers] = useState([]);

  // Form Data
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [manufacturerId, setManufacturerId] = useState("");

  // Handlers: onChange (helper functions)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleManufacturerIdChange = (event) => {
    const value = event.target.value;
    setManufacturerId(value);
  };

  // Handler: onSubmit (helper function)
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    console.log("~~~~", data);
    data.name = name;
    data.picture_url = pictureUrl;
    data.manufacturer_id = Number(manufacturerId);

    const vehicleModelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(vehicleModelUrl, fetchConfig);
    if (response.ok) {
      await response.json();

      setName("");
      setPictureUrl("");
      setManufacturerId("");
    }
  };

  // Fetch Manufacturers (helper function)
  const fetchManufacturers = async () => {
    const manufacturersUrl = "http://localhost:8100/api/manufacturers/";

    const response = await fetch(manufacturersUrl);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  // useEffects do something at a given time
  useEffect(
    // The something is the first parameter, an anonymous function
    () => {
      fetchManufacturers();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  // JSX
  return (
    <div className="create-vehicle-model-container">
      <h2>Create a vehicle model</h2>

      <form className="create-vehicle-model-form" onSubmit={handleSubmit}>
        {/* Name field */}
        <input
          value={name}
          onChange={handleNameChange}
          placeholder="Model name..."
          required
          type="text"
          name="name"
          id="name"
          className="form-control"
        />

        {/* Picture URL field */}
        <input
          value={pictureUrl}
          onChange={handlePictureUrlChange}
          placeholder="Picture URL..."
          required
          type="text"
          name="pictureUrl"
          id="pictureUrl"
          className="form-control"
        />

        {/* Manufacturer field */}
        <label htmlFor="manufacturer"></label>
        <select
          value={manufacturerId}
          onChange={handleManufacturerIdChange}
          required
          name="manufacturer"
          id="manufacturer"
          className="form-select"
        >
          <option value="">Choose a manufacturer</option>
          {manufacturers.map((manufacturer) => {
            return (
              <option key={manufacturer.id} value={manufacturer.id}>
                {manufacturer.name}
              </option>
            );
          })}
        </select>

        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  );
};
