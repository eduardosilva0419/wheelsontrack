import React, { useState } from "react";

export const CreateManufacturer = () => {
  // Form Data
  const [name, setName] = useState("");

  // Handlers: onChange (helper functions)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  // Handler: onSubmit (helper function)
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      await response.json();

      setName("");
    }
  };

  // JSX
  return (
    <div className="add-manufacturer-container">
      <h2>Create a manufacturer</h2>

      <form className="add-manufacturer-form" onSubmit={handleSubmit}>
        {/* First name field */}
        <input
          value={name}
          onChange={handleNameChange}
          placeholder="Manufacturer name..."
          required
          type="text"
          name="name"
          id="name"
          className="form-control"
        />

        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  );
};
