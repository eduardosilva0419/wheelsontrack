import React, { useEffect, useState } from "react";

export const ListAutomobiles = () => {
	const [automobiles, setAutomobiles] = useState([]);

	const fetchAutomobiles = async () => {
		const url = "http://localhost:8100/api/automobiles/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.autos); // Use data.autos instead of data.manufacturers
		}
	};

	useEffect(() => {
		fetchAutomobiles();
	}, []);

	return (
		<div className="list-sales-container">
			<h1>Automobiles</h1>

			<table className="table table-striped manufacturers-list-table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Color</th>
						<th>Year</th>
						<th>VIN</th>
						<th>Model</th>
						<th>Sold</th>
					</tr>
				</thead>

				<tbody>
					{automobiles.map((auto) => (
						<tr key={auto.id}>
							<td>{auto.id}</td>
							<td>{auto.color}</td>
							<td>{auto.year}</td>
							<td>{auto.vin}</td>
							<td>{auto.model.name}</td>
							<td>{auto.sold ? "Yes" : "No"}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};
