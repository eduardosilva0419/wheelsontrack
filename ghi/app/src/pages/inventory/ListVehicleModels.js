import React, { useEffect, useState } from "react";

export const ListVehicleModels = () => {
  const [vehicleModels, setVehicleModels] = useState([]);

  const fetchVehicleModels = async () => {
    const vehicleModelsUrl = "http://localhost:8100/api/models/";

    const response = await fetch(vehicleModelsUrl);

    if (response.ok) {
      const data = await response.json();
      setVehicleModels(data.models);
    }
  };

  // useEffects have two parameters: (1) an anonymous function and (2) a dependency array
  // useEffects do something at a given time
  useEffect(
    () => {
      // The something is the first parameter, an anonymous function
      fetchVehicleModels();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  return (
    <div className="list-vehicle-models-container">
      <h1>Models</h1>

      <table className="table table-striped vehicle-models-list-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>

        <tbody>
          {vehicleModels?.map((vehicleModels) => {
            return (
              <tr key={vehicleModels.name}>
                <td>{vehicleModels.name}</td>
                <td>{vehicleModels.manufacturer.name}</td>
                <td>
                  <img
                    className="list-vehicles-picture"
                    src={vehicleModels.picture_url}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
