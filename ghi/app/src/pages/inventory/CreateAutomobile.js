import React, { useState, useEffect } from "react";

export const CreateAutomobile = () => {
	const [color, setColor] = useState("");
	const [year, setYear] = useState("");
	const [vin, setVin] = useState("");
	const [modelId, setModelId] = useState("");
	const [vehicleModels, setVehicleModels] = useState([]);

	const fetchVehicleModels = async () => {
		try {
			const response = await fetch("http://localhost:8100/api/models/");
			if (response.ok) {
				const data = await response.json();
				setVehicleModels(data.models);
			}
		} catch (error) {
			console.error("Error fetching vehicle models:", error);
		}
	};

	useEffect(() => {
		fetchVehicleModels();
	}, []);

	const handleColorChange = (event) => {
		const value = event.target.value;
		setColor(value);
	};

	const handleYearChange = (event) => {
		const value = event.target.value;
		setYear(value);
	};

	const handleVinChange = (event) => {
		const value = event.target.value;
		setVin(value);
	};

	const handleModelIdChange = (event) => {
		const value = event.target.value;
		setModelId(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			color: color,
			year: parseInt(year),
			vin: vin,
			model_id: parseInt(modelId),
		};

		const url = "http://localhost:8100/api/automobiles/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			await response.json();

			setColor("");
			setYear("");
			setVin("");
			setModelId("");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4 forms">
					<div className="add-automobile-container">
						<h2>Add an automobile to inventory</h2>

						<form className="add-automobile-form" onSubmit={handleSubmit}>
							<div className="form-floating mb-3">
								<input
									value={color}
									onChange={handleColorChange}
									placeholder="Color..."
									required
									type="text"
									name="color"
									id="color"
									className="form-control"
								/>
								<label>Vehicle Color</label>
							</div>
							<div className="form-floating mb-3">
								<input
									value={year}
									onChange={handleYearChange}
									placeholder="Year..."
									required
									type="number"
									name="year"
									id="year"
									className="form-control"
								/>
								<label>Year</label>
							</div>
							<div className="form-floating mb-3">
								<input
									value={vin}
									onChange={handleVinChange}
									placeholder="VIN..."
									required
									type="text"
									name="vin"
									id="vin"
									className="form-control"
								/>
								<label>VIN</label>
							</div>
							<div className="form-floating mb-3">
								<select
									value={modelId}
									onChange={handleModelIdChange}
									required
									name="modelId"
									id="modelId"
									className="form-select"
								>
									<option value="">Choose a Model</option>
									{vehicleModels.map((model) => (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									))}
								</select>
								<label htmlFor="modelId">Model ID</label>
							</div>

							<button type="submit" className="btn btn-primary">
								Create
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateAutomobile;
