import React, { useEffect, useState } from "react";

export const ListTechnicians = () => {
  const [technicians, setTechnicians] = useState([]);

  const fetchTechnicians = async () => {
    const techniciansUrl = "http://localhost:8080/api/technicians/";

    const response = await fetch(techniciansUrl);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(sortTechniciansByEmployeeId(data.technicians));
    }
  };

  // sorting technicians in ascending numerical order by employee id
  const sortTechniciansByEmployeeId = (technicians) => {
    return [...technicians].sort((a, b) => a.employee_id - b.employee_id);
  };

  // call the api endpoint to delete a technician
  const deleteTechnician = async (id) => {
    const deleteTechnicianUrl = `http://localhost:8080/api/technicians/${id}/`;

    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch(deleteTechnicianUrl, fetchConfig);
    // If delete is successful, fetchTechnicians will refresh the page
    if (response.ok) {
      fetchTechnicians();
    }
  };

  // Used in my delete button in my return
  const handleDeleteClick = (id) => {
    deleteTechnician(id);
  };

  // useEffects have two parameters: (1) an anonymous function and (2) a dependency array
  // useEffects do something at a given time
  useEffect(
    () => {
      // The something is the first parameter, an anonymous function
      fetchTechnicians();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  return (
    <div className="list-technician-container">
      <h1>Technicians</h1>

      <table className="table table-striped technicians-list-table">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>

        <tbody>
          {technicians.map((technician) => {
            return (
              <tr key={technician.employee_id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDeleteClick(technician.employee_id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
