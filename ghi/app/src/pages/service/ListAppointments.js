import React, { useEffect, useState } from "react";

export const ListAppointments = () => {
  const [appointments, setAppointments] = useState([]);
  // used for isVIP
  const [automobiles, setAutomobiles] = useState([]);

  const fetchAppointments = async () => {
    const ListAppointmentsUrl = "http://localhost:8080/api/appointments/";

    const response = await fetch(ListAppointmentsUrl);

    if (response.ok) {
      const data = await response.json();
      setAppointments(sortAppointmentsByVIN(data.appointments));
    }
  };

  // sorting appointments alphabetically by VIN number
  const sortAppointmentsByVIN = (appointments) => {
    return [...appointments].sort((a, b) => a.vin.localeCompare(b.vin));
  };

  // call the api endpoint to get the list of automobiles and VIN numbers
  const fetchAutomobiles = async () => {
    const ListAutomobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(ListAutomobilesUrl);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  // call the api endpoint to cancel an appointment
  const cancelAppointment = async (id) => {
    const cancelAppointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel`;

    const fetchConfig = {
      method: "put",
    };

    const response = await fetch(cancelAppointmentUrl, fetchConfig);
    // If cancel is successful, fetchAppointments will refresh the page
    if (response.ok) {
      fetchAppointments();
    }
  };

  // call the api endpoint to cancel an appointment
  const finishAppointment = async (id) => {
    const finishAppointmentUrl = `http://localhost:8080/api/appointments/${id}/finish`;

    const fetchConfig = {
      method: "put",
    };

    const response = await fetch(finishAppointmentUrl, fetchConfig);
    // If finish is successful, fetchAppointments will refresh the page
    if (response.ok) {
      fetchAppointments();
    }
  };

  // Used in cancel button in the JSX
  const handleCancelClick = (id) => {
    cancelAppointment(id);
  };

  // Used in finish button in the JSX
  const handleFinishClick = (id) => {
    finishAppointment(id);
  };

  // useEffects have two parameters: (1) an anonymous function and (2) a dependency array
  // useEffects do something at a given time
  useEffect(
    () => {
      // The something is the first parameter, an anonymous function
      fetchAppointments();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  return (
    <div className="list-appointments-container">
      <h1>Service Appointments</h1>

      <table className="table table-striped appointments-list-table">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody>
          {appointments.map((appointment) => {
            // check if the appointment's VIN exists in the list of automobiles
            // https://shorturl.at/oFMWY and https://shorturl.at/mJY35
            const isVIP = automobiles.some(
              (auto) => auto.vin === appointment.vin
            );

            // extract date and time components from Appointment model's date_time property
            const appointmentDate = new Date(
              appointment.date_time
            ).toLocaleDateString();
            const appointmentTime = new Date(
              appointment.date_time
            ).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" });

            return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td>{isVIP ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{appointmentDate}</td>
                <td>{appointmentTime}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleCancelClick(appointment.vin)}
                  >
                    Cancel
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => handleFinishClick(appointment.vin)}
                  >
                    Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
