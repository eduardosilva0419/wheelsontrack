import React, { useEffect, useState } from "react";

export const ListServiceHistory = () => {
  const [serviceHistory, setServiceHistory] = useState([]);
  // used for isVIP
  const [automobiles, setAutomobiles] = useState([]);
  // used for filtering by vin
  const [searchVIN, setSearchVIN] = useState("");

  // call the api endpoint to get the list of automobiles and VIN numbers
  const fetchServiceHistory = async () => {
    const serviceHistoryUrl = "http://localhost:8080/api/appointments/";

    const response = await fetch(serviceHistoryUrl);

    if (response.ok) {
      const data = await response.json();
      setServiceHistory(data.appointments);
    }
  };

  const fetchAutomobiles = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobilesUrl);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  // handle VIN search form submission
  const handleSearchVIN = (event) => {
    event.preventDefault();

    // filter serviceHistory based on VIN
    const filteredAppointments = serviceHistory.filter(
      (appointment) => appointment.vin === searchVIN
    );
    setServiceHistory(filteredAppointments);
  };

  // handle VIN search input change
  const handleSearchInputChange = (event) => {
    setSearchVIN(event.target.value);
  };

  // handle reset of the search
  const handleResetSearch = () => {
    fetchServiceHistory(); // Reset the service history to its original state
    setSearchVIN(""); // Clear the search VIN input
  };

  // useEffects have two parameters: (1) an anonymous function and (2) a dependency array
  // useEffects do something at a given time
  useEffect(
    () => {
      // The something is the first parameter, an anonymous function
      fetchServiceHistory();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  return (
    <div className="list-service-history-container">
      <h1>Service History</h1>

      {/* search form to filter by VIN */}
      <form className="filter-service-history" onSubmit={handleSearchVIN}>
        <input
          type="text"
          value={searchVIN}
          onChange={handleSearchInputChange}
          placeholder="Enter VIN to search..."
        />
        <button type="submit">Search</button>
        <button type="button" onClick={handleResetSearch}>
          Reset
        </button>
      </form>

      <table className="table table-striped service-history-list-table">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody>
          {serviceHistory.map((appointment) => {
            // check if the appointment's VIN exists in the list of automobiles
            // https://shorturl.at/oFMWY and https://shorturl.at/mJY35
            const isVIP = automobiles.some(
              (auto) => auto.vin === appointment.vin
            );

            // extract date and time components from Appointment model's date_time property
            const appointmentDate = new Date(
              appointment.date_time
            ).toLocaleDateString();
            const appointmentTime = new Date(
              appointment.date_time
            ).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" });

            return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td>{isVIP ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{appointmentDate}</td>
                <td>{appointmentTime}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
