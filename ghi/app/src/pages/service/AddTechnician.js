import React, { useState } from "react";

export const AddTechnician = () => {
  // Form Data
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");

  // Handlers: onChange (helper functions)
  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  // Handler: onSubmit (helper function)
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      await response.json();

      setFirstName("");
      setLastName("");
      setEmployeeId("");
    }
  };

  // JSX
  return (
    <div className="add-technician-container">
      <h2>Add a Technician</h2>

      <form className="add-technician-form" onSubmit={handleSubmit}>
        {/* First name field */}
        <input
          value={firstName}
          onChange={handleFirstNameChange}
          placeholder="First name..."
          required
          type="text"
          name="firstName"
          id="firstName"
          className="form-control"
        />

        {/* Last name field */}
        <input
          value={lastName}
          onChange={handleLastNameChange}
          placeholder="Last name..."
          required
          type="text"
          name="lastName"
          id="lastName"
          className="form-control"
        />

        {/* Employee ID field */}
        <input
          value={employeeId}
          onChange={handleEmployeeIdChange}
          placeholder="Employee ID..."
          required
          type="text"
          name="employeeId"
          id="employeeId"
          className="form-control"
        />

        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  );
};
