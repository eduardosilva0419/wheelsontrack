import React, { useEffect, useState } from "react";

export const CreateAppointment = () => {
  // Technician Selection Data
  const [technicians, setTechnicians] = useState([]);

  // Form Data
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");

  // Handlers: onChange (helper functions)
  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleDateChange = (event) => {
    const value = event.target.value;
    setDate(value);
  };

  const handleTimeChange = (event) => {
    const value = event.target.value;
    setTime(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  // Handler: onSubmit (helper function)
  const handleSubmit = async (event) => {
    event.preventDefault();

    const fullDate = `${date}T${time}`;

    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.date_time = fullDate;
    data.technician = technician;
    data.reason = reason;

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      await response.json();

      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setTechnician("");
      setReason("");
    }
  };

  // Fetch Technicians (helper function)
  const fetchTechnicians = async () => {
    const techniciansUrl = "http://localhost:8080/api/technicians/";

    const response = await fetch(techniciansUrl);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  // useEffects do something at a given time
  useEffect(
    // The something is the first parameter, an anonymous function
    () => {
      fetchTechnicians();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  // JSX
  return (
    <div className="create-appointment-container">
      <h2>Create a service appointment</h2>

      <form className="create-appointment-form" onSubmit={handleSubmit}>
        {/* vin field */}
        <label htmlFor="vin">Automobile VIN</label>
        <input
          value={vin}
          onChange={handleVinChange}
          required
          type="text"
          name="vin"
          id="vin"
          className="form-control"
        />

        {/* customer field */}
        <label htmlFor="customer">Customer</label>
        <input
          value={customer}
          onChange={handleCustomerChange}
          required
          type="text"
          name="customer"
          id="customer"
          className="form-control"
        />

        {/* date field */}
        <label htmlFor="date">Date</label>
        <input
          value={date}
          onChange={handleDateChange}
          required
          type="date"
          name="date"
          id="date"
          className="form-control"
        />

        {/* time field */}
        <label htmlFor="time">Time</label>
        <input
          value={time}
          onChange={handleTimeChange}
          placeholder="Time..."
          required
          type="time"
          name="time"
          id="time"
          className="form-control"
        />

        {/* technician field */}
        <label htmlFor="technician">Technician</label>
        <select
          value={technician}
          onChange={handleTechnicianChange}
          required
          name="technician"
          id="technician"
          className="form-select"
        >
          <option value="">Choose a technician</option>
          {technicians.map((technician) => {
            return (
              <option
                key={technician.employee_id}
                value={technician.employee_id}
              >
                {`${technician.first_name} ${technician.last_name}`}
              </option>
            );
          })}
        </select>

        {/* reason field */}
        <label htmlFor="reason">Reason</label>
        <input
          value={reason}
          onChange={handleReasonChange}
          required
          type="text"
          name="reason"
          id="reason"
          className="form-control"
        />

        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  );
};
