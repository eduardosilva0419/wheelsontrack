import React, { useState } from "react";

export const SalespersonForm = () => {
	const [first_name, setFirstName] = useState("");
	const [last_name, setLastName] = useState("");
	const [employee_id, setEmployee_Id] = useState("");

	const handleFirstNameChange = (event) => {
		const value = event.target.value;
		setFirstName(value);
	};
	const handleLastNameChange = (event) => {
		const value = event.target.value;
		setLastName(value);
	};

	const handleEmployee_IdChange = (event) => {
		const value = event.target.value;
		setEmployee_Id(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.first_name = first_name;
		data.last_name = last_name;
		data.employee_id = employee_id;

		const url = "http://localhost:8090/api/salespeople/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			await response.json();

			setFirstName("");
			setLastName("");
			setEmployee_Id("");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4 forms">
					<h1>Add a Salesperson</h1>
					<form onSubmit={handleSubmit} id="create-Salesperson-form">
						<div className="form-floating mb-3">
							<input
								value={first_name}
								onChange={handleFirstNameChange}
								required
								type="text"
								name="first_name"
								id="first_name"
								className="form-control"
							/>
							<label>First Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={last_name}
								onChange={handleLastNameChange}
								required
								type="text"
								name="last_name"
								id="last_name"
								className="form-control"
							/>
							<label>Last Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={employee_id}
								onChange={handleEmployee_IdChange}
								required
								type="text"
								name="employee_id"
								id="employee_id"
								className="form-control"
							/>
							<label>Employee Id</label>
						</div>
						<button className="btn btn-dark buttons">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default SalespersonForm;
