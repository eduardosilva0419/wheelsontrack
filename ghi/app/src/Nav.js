import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-dark bg-success">
      <NavLink className="navbar-brand" to="/">
        CarCar
      </NavLink>
      {/* ------------------------------- Inventory Nav links ------------------------------- */}
      {/* ListManufacturers.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/manufacturers/list"
        >
          Manufacturers
        </NavLink>
      </div>

      {/* CreateManufacturers.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/manufacturers/create"
        >
          Create a Manufacturer
        </NavLink>
      </div>

      {/* ListVehicleModels.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/vehiclemodels/list"
        >
          Models
        </NavLink>
      </div>

      {/* CreateVehicleModels.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/vehiclemodels/create"
        >
          Create a Model
        </NavLink>
      </div>

      {/* ListAutomobiles.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/automobiles/list"
        >
          Automobiles
        </NavLink>
      </div>

      {/* CreateAutomobiles.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/inventory/automobiles/create"
        >
          Create an Automobile
        </NavLink>
      </div>

      {/* ------------------------------- Sales Nav links ------------------------------- */}

      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/salesperson/list"
        >
          Salespeople
        </NavLink>
      </div>
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/salesperson/new"
        >
          Add a Salesperson
        </NavLink>
      </div>
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/customers/list"
        >
          Customers
        </NavLink>
      </div>

      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/customers/new"
        >
          Add a Customer
        </NavLink>
      </div>
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/sales/list"
        >
          Sales
        </NavLink>
      </div>
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/sales/new"
        >
          Add a Sale
        </NavLink>
      </div>
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/sales/history"
        >
          Salesperson History
        </NavLink>
      </div>

      {/* ------------------------------- Service Nav links ------------------------------- */}

      {/* ListTechnicians.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/service/technicians/list"
        >
          Technicians
        </NavLink>
      </div>

      {/* AddTechnician.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/service/technicians/add"
        >
          Add a Technician
        </NavLink>
      </div>

      {/* ListAppointments.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/service/appointments/list"
        >
          Service Appointments
        </NavLink>
      </div>

      {/* CreateAppointment.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/service/appointments/create"
        >
          Create a Service Appointment
        </NavLink>
      </div>

      {/* ListServiceHistory.js */}
      <div className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/service/appointments/history"
        >
          Service History
        </NavLink>
      </div>
    </nav>
  );
}

export default Nav;
