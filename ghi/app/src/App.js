import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespersonForm from "./SalespersonForm";
import SalespersonList from "./SalespersonList";
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import SalesRecordForm from "./SalesRecordForm";
import SalesRecordList from "./SalesRecordList";
import SalespersonHistory from "./SalespersonHistory";
import { AddTechnician } from "./pages/service/AddTechnician";
import { ListTechnicians } from "./pages/service/ListTechnicians";
import { CreateAppointment } from "./pages/service/CreateAppointment";
import { ListAppointments } from "./pages/service/ListAppointments";
import { ListServiceHistory } from "./pages/service/ListServiceHistory";
import { ListManufacturers } from "./pages/inventory/ListManufacturers";
import { CreateManufacturer } from "./pages/inventory/CreateManufacturer";
import { ListVehicleModels } from "./pages/inventory/ListVehicleModels";
import { CreateVehicleModel } from "./pages/inventory/CreateVehicleModel";
import { ListAutomobiles } from "./pages/inventory/ListAutomobiles";
import { CreateAutomobile } from "./pages/inventory/CreateAutomobile";

function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />

          {/* -------- inventory routes -------- */}
          <Route path="/inventory">
            {/* list manufacturers route */}
            <Route
              path="/inventory/manufacturers/list"
              element={<ListManufacturers />}
            />

            {/* create manufacturer route */}
            <Route
              path="/inventory/manufacturers/create"
              element={<CreateManufacturer />}
            />

            {/* list vehicle model route */}
            <Route
              path="/inventory/vehiclemodels/list"
              element={<ListVehicleModels />}
            />

            {/* create vehicle model route */}
            <Route
              path="/inventory/vehiclemodels/create"
              element={<CreateVehicleModel />}
            />

            {/* list automobiles route */}
            <Route
              path="/inventory/automobiles/list"
              element={<ListAutomobiles />}
            />

            {/* create automobile route */}
            <Route
              path="/inventory/automobiles/create"
              element={<CreateAutomobile />}
            />
          </Route>

					{/* -------- sales-eduardo routes -------- */}
					<Route path="salesperson">
						<Route path="new" element={<SalespersonForm />} />
						<Route path="list" element={<SalespersonList />} />
					</Route>
					<Route path="customers">
						<Route path="new" element={<CustomerForm />} />
						<Route path="list" element={<CustomerList />} />
					</Route>
					<Route path="sales">
						<Route path="new" element={<SalesRecordForm />} />
						<Route path="list" element={<SalesRecordList />} />
						<Route path="history" element={<SalespersonHistory />} />
					</Route>

					{/* -------- service-brandon routes -------- */}
					<Route path="/service">
						{/* add technician route */}
						<Route
							path="/service/technicians/add"
							element={<AddTechnician />}
						/>

						{/* list technicians route */}
						<Route
							path="/service/technicians/list"
							element={<ListTechnicians />}
						/>

						{/* create appointment route */}
						<Route
							path="/service/appointments/create"
							element={<CreateAppointment />}
						/>

						{/* list appointments route */}
						<Route
							path="/service/appointments/list"
							element={<ListAppointments />}
						/>

						{/* list service history route */}
						<Route
							path="/service/appointments/history"
							element={<ListServiceHistory />}
						/>
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
