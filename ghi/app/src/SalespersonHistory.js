import React, { useState, useEffect } from "react";

export const SalespersonHistory = () => {
	const [sales, setSales] = useState([]);
	const [salespeople, setSalespeople] = useState([]);
	const [selectedSalespersonId, setSelectedSalespersonId] = useState("");

	const fetchSales = async () => {
		const url = "http://localhost:8090/api/sales/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setSales(data.salesrecord);
		}
	};

	const fetchSalespeople = async () => {
		const url = "http://localhost:8090/api/salespeople/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople);
		}
	};

	useEffect(() => {
		fetchSales();
		fetchSalespeople();
	}, []);

	const handleSalespersonChange = (event) => {
		setSelectedSalespersonId(event.target.value);
	};

	const filteredSales = sales.filter((sale) => {
		return (
			selectedSalespersonId === "" ||
			sale.salesperson.id === Number(selectedSalespersonId)
		);
	});

	return (
		<div className="list-sales-container">
			<h1>Salesperson History</h1>
			<label>Select Salesperson: </label>
			<select value={selectedSalespersonId} onChange={handleSalespersonChange}>
				<option value="">All Salespeople</option>
				{salespeople.map((salesperson) => (
					<option key={salesperson.id} value={salesperson.id}>
						{`${salesperson.first_name} ${salesperson.last_name}`}
					</option>
				))}
			</select>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Salesperson Name</th>
						<th>Customer</th>
						<th>VIN</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{filteredSales.map((sale) => (
						<tr key={sale.id}>
							<td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
							<td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
							<td>{sale.automobile.vin}</td>
							<td>{sale.price}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default SalespersonHistory;
