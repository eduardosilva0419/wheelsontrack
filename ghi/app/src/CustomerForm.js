import React, { useState } from "react";

export const CustomerForm = () => {
	const [first_name, setFirstName] = useState("");
	const [last_name, setLastName] = useState("");
	const [address, setAddress] = useState("");
	const [phone, setPhone] = useState("");

	const handleFirstNameChange = (event) => {
		const value = event.target.value;
		setFirstName(value);
	};
	const handleLastNameChange = (event) => {
		const value = event.target.value;
		setLastName(value);
	};

	const handleAddressChange = (event) => {
		const value = event.target.value;
		setAddress(value);
	};

	const handlePhoneChange = (event) => {
		const value = event.target.value;
		setPhone(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.first_name = first_name;
		data.last_name = last_name;
		data.address = address;
		data.phone = phone;

		const url = "http://localhost:8090/api/customers/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			await response.json();

			setFirstName("");
			setLastName("");
			setAddress("");
			setPhone("");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4 forms">
					<h1>Add a Customer</h1>
					<form onSubmit={handleSubmit} id="create-Salesperson-form">
						<div className="form-floating mb-3">
							<input
								value={first_name}
								onChange={handleFirstNameChange}
								required
								type="text"
								name="first_name"
								id="first_name"
								className="form-control"
							/>
							<label>First Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={last_name}
								onChange={handleLastNameChange}
								required
								type="text"
								name="last_name"
								id="last_name"
								className="form-control"
							/>
							<label>Last Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={address}
								onChange={handleAddressChange}
								required
								type="text"
								name="address"
								id="address"
								className="form-control"
							/>
							<label>Address</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={phone}
								onChange={handlePhoneChange}
								required
								type="text"
								name="phone"
								id="phone"
								className="form-control"
							/>
							<label>Phone</label>
						</div>
						<button className="btn btn-dark buttons">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default CustomerForm;
