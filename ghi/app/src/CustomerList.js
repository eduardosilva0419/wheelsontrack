import React, { useState, useEffect } from "react";

export const CustomersList = () => {
	const [customers, setCustomers] = useState([]);

	const fetchCustomers = async () => {
		const url = "http://localhost:8090/api/customers/";

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers);
		}
	};

	useEffect(() => {
		fetchCustomers();
	}, []);

	return (
		<div className="list-sales-container">
			<h1>Customers</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>First name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Phone</th>
					</tr>
				</thead>
				<tbody>
					{customers.map((customers) => {
						return (
							<tr key={customers.id}>
								<td>{customers.first_name}</td>
								<td>{customers.last_name}</td>
								<td>{customers.address}</td>
								<td>{customers.phone}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default CustomersList;
