import React, { useState, useEffect } from "react";
// import "./index.css";

export const SalespersonList = () => {
	const [salespeople, setSalespeople] = useState([]);

	const fetchSalespeople = async () => {
		const url = "http://localhost:8090/api/salespeople/";

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople);
		}
	};

	const deleteSalesperson = async (id) => {
		const url = `http://localhost:8090/api/salespeople/${id}`;

		const fetchConfig = {
			method: "delete",
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchSalespeople();
		}
	};

	const handleDelete = (id) => {
		deleteSalesperson(id);
	};

	useEffect(() => {
		fetchSalespeople();
	}, []);

	return (
		<div className="list-sales-container">
			<h1>Salespeople</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody>
					{salespeople?.map((salesperson) => {
						return (
							<tr key={salesperson.id}>
								<td>{salesperson.first_name}</td>
								<td>{salesperson.last_name}</td>
								<td>
									<button onClick={() => handleDelete(salesperson.id)}>
										Delete
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default SalespersonList;
