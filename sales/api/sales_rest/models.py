from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=12)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})


class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO, related_name="SalesRecords", on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Salesperson, related_name="SalesRecords", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="SalesRecords", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.salesperson.first_name

    def get_api_url(self):
        return reverse("api_show_sales_record", kwargs={"pk": self.pk})
