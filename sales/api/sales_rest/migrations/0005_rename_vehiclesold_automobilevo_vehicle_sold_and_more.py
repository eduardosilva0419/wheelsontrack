# Generated by Django 4.0.3 on 2023-07-25 21:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0004_rename_import_href_automobilevo_href'),
    ]

    operations = [
        migrations.RenameField(
            model_name='automobilevo',
            old_name='vehicleSold',
            new_name='vehicle_sold',
        ),
        migrations.RemoveField(
            model_name='automobilevo',
            name='color',
        ),
        migrations.RemoveField(
            model_name='automobilevo',
            name='href',
        ),
        migrations.RemoveField(
            model_name='automobilevo',
            name='make',
        ),
        migrations.RemoveField(
            model_name='automobilevo',
            name='year',
        ),
    ]
