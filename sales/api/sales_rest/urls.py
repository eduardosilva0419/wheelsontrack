from django.urls import path
from .views import (
    api_list_automobileVO,
    api_list_customers,
    api_list_salespersons,
    api_show_customer,
    api_show_salesperson,
    api_list_sales_records,
)


urlpatterns = [
    path("salespeople/", api_list_salespersons, name="api_create_salesperson"),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_list_customers, name="api_create_customer"),
    path("automobiles/", api_list_automobileVO, name="api_list_automobiles"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sales_records, name="api_create_sales_records"),
    
    # path("sales/", api_list_sales_records, name="api_list_sales_records"),
]
