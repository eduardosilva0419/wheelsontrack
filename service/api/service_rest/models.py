from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(
        unique=True
    )  # https://shorturl.at/msuEY

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    # The reverse function generates a url using the URL pattern name
    # shoes_rest/urls.py, which becomes the href inside the JsonResponse of
    # the view functions.
    # It is called reverse because it takes an input of a url name and gives
    # the actual url, which is reverse to having a url first
    # and then give it a name. https://shorturl.at/hkAF2.
    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()  # TextField: https://shorturl.at/lqGKN
    status = models.CharField(max_length=100, default="Pending")
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.date_time}"

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})
