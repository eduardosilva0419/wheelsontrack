from django.urls import path
from .views import (
    list_technicians,
    show_technician,
    list_appointments,
    show_appointment,
    cancel_appointment,
    finish_appointment,
)

urlpatterns = [
    # Lists all technicians
    path("technicians/", list_technicians, name="create_technician"),
    # Shows details of specific technician based on its id
    path("technicians/<int:employee_id>/", show_technician, name="show_technician"),
    # Lists all appointments
    path("appointments/", list_appointments, name="create_appointment"),
    # Shows details of specific appointment based on its id
    path("appointments/<str:vin>/", show_appointment, name="show_appointment"),
    # Marks status of an appointment as "Canceled" based on its id
    path(
        "appointments/<str:vin>/cancel", cancel_appointment, name="cancel_appointment"
    ),
    # Marks status of an appointment as "Finished" based on its id
    path(
        "appointments/<str:vin>/finish", finish_appointment, name="finish_appointment"
    ),
]
