from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

    # def get_extra_data(self, o):
    #     return {"closet_name": o.bin.closet_name}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        # generic exception class https://shorturl.at/bfu49
        except Exception:
            response = JsonResponse(
                {"message": "Could not create technician, ensure Employee ID is unique"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def show_technician(request, employee_id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_id=employee_id)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not find a technician with that Employee ID"}
            )
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=employee_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(employee_id=employee_id)

            props = [
                "first_name",
                "last_name",
                "employee_id",
            ]

            for prop in props:
                if prop in content:
                    # Checks the content of said prop(s)
                    # and then sets the new attribute(s).
                    # If there is no change to a prop,
                    # it remains the same.
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_appointments(request, technician_vo_id=None):
    if request.method == "GET":
        if technician_vo_id is not None:
            appointments = Appointment.objects.filter(technician=technician_vo_id)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the technician object and put it in the content dict
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician employee id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_appointment(request, vin):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(vin=vin)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment id"},
                status=404,
            )

    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(vin=vin).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # PUT
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(vin=vin)

            props = [
                "date_time",
                "reason",
                "status",
                "vin",
                "customer",
                "technician",
            ]

            for prop in props:
                if prop in content:
                    # Checks the content of said prop(s)
                    # and then sets the new attribute(s).
                    # If there is no change to a prop,
                    # it remains the same.
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def cancel_appointment(request, vin):
    try:
        appointment = Appointment.objects.get(vin=vin)

        setattr(appointment, "status", "Canceled")
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def finish_appointment(request, vin):
    try:
        appointment = Appointment.objects.get(vin=vin)

        setattr(appointment, "status", "Finished")
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
